## [1.9.3] - 2024-12-27
### Fixed
- warning in plugin upgrade watcher

## [1.9.2] - 2024-12-05
### Fixed
- infinite upgrade popup display

## [1.9.1] - 2024-10-22
### Fixed
- wp-forms v 2 compatibility

## [1.9.0] - 2024-10-21
### Added
- Live rates upgrade message factory

## [1.8.1] - 2024-10-21
### Fixed
- compatibility with wp-forms v 2

## [1.8.0] - 2024-10-21
### Changed
- allowed wp-forms v 2

## [1.7.1] - 2024-10-21
### Added
- icons images

## [1.7.0] - 2023-07-10
### Added
- icons images

## [1.6.1] - 2023-04-18
### Fixed
- return type for jsonSerialize method

## [1.6.0] - 2022-12-16
### Added
- plugin update onboarding tracker

## [1.5.5] - 2022-09-29
### Fixed
- infinite popup display

## [1.5.3] - 2022-08-31
### Fixed
- show upgrade on old installation 

## [1.5.2] - 2022-08-30
### Fixed
- translations

## [1.5.0] - 2022-08-29
### Added
- plugin upgrade onboarding

## [1.4.1] - 2022-08-26
### Fixed
- views counter

## [1.4.0] - 2022-08-25
### Added
- onboarding allowed filter

## [1.3.1] - 2022-08-25
### Fixed
- display strategy

## [1.3.0] - 2022-08-24
### Added
- modal can be closed only by buttons

## [1.2.1] - 2022-08-22
### Fixed
- scss file

## [1.2.0] - 2022-08-22
### Added
- deactivation tracker data

## [1.1.0] - 2022-08-22
### Added
- tracker data

## [1.0.0] - 2022-08-11
### Added
- initial version
