[![pipeline status](https://gitlab.com/wpdesk//predators/library/wp-onboarding/badges/main/pipeline.svg)](https://gitlab.com/wpdesk/predators/library/wp-onboarding/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/predators/library/wp-onboarding/badges/main/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/predators/library/wp-onboarding/commits/main) 
[![Latest Stable Version](https://poser.pugx.org/octolize/wp-onboarding/v/stable)](https://packagist.org/packages/octolize/wp-onboarding) 
[![Total Downloads](https://poser.pugx.org/octolize/wp-onboarding/downloads)](https://packagist.org/packages/octolize/wp-onboarding) 
[![License](https://poser.pugx.org/octolize/wp-onboarding/license)](https://packagist.org/packages/octolize/wp-onboarding) 

# WP Onboarding

Displays onboarding popups.

## Developers

### Assets 

Before commit run:
```shell
npm run prod
```

### Versioning

**It is important to keep same versions in PHP and REACT scripts.**

Version for PHP scripts is stored in src/Onboarding.php

Version for REACT scripts is stored in assets-src/js/index.jsx
